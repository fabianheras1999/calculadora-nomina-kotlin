package com.example.calculadoranomina

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class ReciboNominaActivity : AppCompatActivity() {
    // Declaración de variables de componentes de la interfaz de usuario
    private lateinit var txtNumRecibo: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtHorasNormales: EditText
    private lateinit var txtHorasExtra: EditText
    private lateinit var rdbPuesto: RadioGroup
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button
    private lateinit var lblSubtotalTotal: TextView
    private lateinit var lblImpuestoTotal: TextView
    private lateinit var lblTotalTotal: TextView
    private lateinit var lblUsuario: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibo_nomina)

        // Inicialización de componentes de la interfaz de usuario
        iniciarComponentes()

        // Obtención del usuario de la actividad anterior
        val datos = intent.extras
        val usuario = datos?.getString("usuario")
        lblUsuario.text = usuario

        // Manejo de eventos de los botones
        btnCalcular.setOnClickListener {
            if (validarCampos()) {
                calcularNomina()
            } else {
                mostrarToast("Por favor, completa todos los campos")
            }
        }

        btnLimpiar.setOnClickListener { limpiarCampos() }
        btnRegresar.setOnClickListener { regresar() }
    }

    private fun iniciarComponentes() {
        // Asignación de componentes de la interfaz a las variables
        txtNumRecibo = findViewById(R.id.txtNumRecibo)
        txtNombre = findViewById(R.id.txtNombre)
        txtHorasNormales = findViewById(R.id.txtHorasNormales)
        txtHorasExtra = findViewById(R.id.txtHorasExtra)
        rdbPuesto = findViewById(R.id.rdbPuesto)
        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        lblSubtotalTotal = findViewById(R.id.lblSubtotalTotal)
        lblImpuestoTotal = findViewById(R.id.lblImpuestoTotal)
        lblTotalTotal = findViewById(R.id.lblTotalTotal)
        lblUsuario = findViewById(R.id.lblUsuario)
    }

    private fun validarCampos(): Boolean {
        // Validación de campos: se verifica si todos los campos están completos
        val numRecibo = txtNumRecibo.text.toString()
        val nombre = txtNombre.text.toString()
        val horasNormales = txtHorasNormales.text.toString()
        val horasExtra = txtHorasExtra.text.toString()
        val selectedRadioButtonId = rdbPuesto.checkedRadioButtonId
        return numRecibo.isNotEmpty() && nombre.isNotEmpty() && horasNormales.isNotEmpty() && horasExtra.isNotEmpty() && selectedRadioButtonId != -1
    }

    private fun calcularNomina() {
        // Cálculo de la nómina en base a los datos ingresados
        val numRecibo = txtNumRecibo.text.toString().toInt()
        val nombre = txtNombre.text.toString()
        val horasNormales = txtHorasNormales.text.toString().toFloat()
        val horasExtra = txtHorasExtra.text.toString().toFloat()
        val selectedRadioButtonId = rdbPuesto.checkedRadioButtonId
        var puesto = 0

        if (selectedRadioButtonId != -1) {
            val selectedRadioButton = findViewById<RadioButton>(selectedRadioButtonId)
            val puestoText = selectedRadioButton.text.toString()

            // Asignación de un valor numérico al puesto según la opción seleccionada
            when (puestoText.toLowerCase()) {
                "auxiliar" -> puesto = 1
                "albañil" -> puesto = 2
                "ing obra" -> puesto = 3
            }
        }

        // Creación de un objeto ReciboNomina y cálculo de los valores
        val reciboNomina = ReciboNomina(numRecibo, nombre, horasNormales, horasExtra, puesto, 16f)
        val subtotal = reciboNomina.calcularSubtotal()
        val impuesto = reciboNomina.calcularImpuesto()
        val total = reciboNomina.calcularTotal()

        // Actualización de las etiquetas de total y subtotal en la interfaz
        lblSubtotalTotal.text = subtotal.toString()
        lblImpuestoTotal.text = impuesto.toString()
        lblTotalTotal.text = total.toString()
    }

    private fun limpiarCampos() {
        // Limpieza de los campos de la interfaz
        txtNumRecibo.setText("")
        txtNombre.setText("")
        txtHorasNormales.setText("")
        txtHorasExtra.setText("")
        rdbPuesto.clearCheck()
        lblSubtotalTotal.text = ""
        lblImpuestoTotal.text = ""
        lblTotalTotal.text = ""
    }

    private fun regresar() {
        // Diálogo de confirmación al regresar al menú principal
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora Nómina")
        confirmar.setMessage("¿Regresar al Menú Principal?")
        confirmar.setPositiveButton("Confirmar") { dialog, _ -> finish() }
        confirmar.setNegativeButton("Cancelar") { dialog, _ -> dialog.dismiss() }
        confirmar.show()
    }

    private fun mostrarToast(mensaje: String) {
        // Mostrar un mensaje Toast en la pantalla
        Toast.makeText(applicationContext, mensaje, Toast.LENGTH_SHORT).show()
    }
}
