package com.example.calculadoranomina

import java.text.DecimalFormat

class ReciboNomina(// Getters y Setters
    var numRecibo: Int,
    var nombre: String,
    var horasTrabNormal: Float,
    var horasTrabExtras: Float,
    var puesto: Int,
    var impuestoPorc: Float
) {

    // Métodos para cálculos
    fun reciboNominal(): Float {
        val pagoBase = 200f
        return when (puesto) {
            1 -> pagoBase * 1.2f
            2 -> pagoBase * 1.5f
            3 -> pagoBase * 2.0f
            else -> pagoBase
        }
    }

    fun calcularSubtotal(): Float {
        val pagoBase = reciboNominal()
        val pagoHoraExtra = pagoBase * 2
        val subtotal = pagoBase * horasTrabNormal + pagoHoraExtra * horasTrabExtras
        return formatFloat(subtotal)
    }

    fun calcularImpuesto(): Float {
        val subtotal = calcularSubtotal()
        val impues = subtotal * (impuestoPorc / 100)
        return formatFloat(impues)
    }

    fun calcularTotal(): Float {
        val subtotal = calcularSubtotal()
        val impuesto = calcularImpuesto()
        val total = subtotal - impuesto
        return formatFloat(total)
    }

    private fun formatFloat(value: Float): Float {
        val decimalFormat = DecimalFormat("#.00")
        return decimalFormat.format(value.toDouble()).toFloat()
    }
}