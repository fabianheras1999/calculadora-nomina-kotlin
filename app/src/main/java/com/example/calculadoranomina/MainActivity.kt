package com.example.calculadoranomina

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    // Declaración de variables para los elementos de la interfaz de usuario
    private var btnIngresar: Button? = null
    private var btnSalir: Button? = null
    private var txtUsuario: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Inicialización de los componentes de la interfaz de usuario
        iniciarComponentes()

        // Asignar listeners a los botones
        btnIngresar!!.setOnClickListener { ingresar() }
        btnSalir!!.setOnClickListener { salir() }
    }

    private fun iniciarComponentes() {
        // Asignar los botones y el campo de texto a las variables correspondientes
        btnSalir = findViewById<Button>(R.id.btnSalir)
        btnIngresar = findViewById<Button>(R.id.btnIngresar)
        txtUsuario = findViewById<EditText>(R.id.txtUsuario)
    }

    private fun ingresar() {
        // Obtener el texto ingresado en el campo de texto txtUsuario
        val strUsuario = txtUsuario!!.text.toString()

        // Crear un objeto Bundle para pasar el valor del usuario a la siguiente actividad
        val bundle = Bundle()
        bundle.putString("usuario", strUsuario)

        // Crear un Intent para iniciar la nueva actividad (ReciboNominaActivity)
        val intent = Intent(this@MainActivity, ReciboNominaActivity::class.java)

        // Asignar el Bundle al Intent
        intent.putExtras(bundle)

        // Iniciar la nueva actividad
        startActivity(intent)
    }

    private fun salir() {
        // Mostrar un cuadro de diálogo de confirmación para salir de la aplicación
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora Nómina")
        confirmar.setMessage("¿Desea salir de la aplicación?")

        // Acción positiva del cuadro de diálogo: cerrar la actividad actual (MainActivity)
        confirmar.setPositiveButton("Confirmar") { dialogInterface: DialogInterface?, which: Int -> finish() }

        // Acción negativa del cuadro de diálogo: no hacer nada
        confirmar.setNegativeButton("Cancelar") { dialogInterface: DialogInterface?, which: Int -> }

        // Mostrar el cuadro de diálogo
        confirmar.show()
    }
}
